<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\ProductImage;
use App\User;
use App\Courier;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();


    }

    public function productSeed()
    {
        $array_product_name = array('Tango Wafer Vanilla 78gr', 'Tango Wafer Vanilla 47gr', 'Kusuka Kripik SIngkong Ayam Lada hitam', 'Kacang Dua Kelinci');
        $array_product_price = array('4500', '2200', '4500', '16000');
        $array_product_sku = array('TSTORE|TSSN033', 'TSTORE|TSSN034', 'TSTORE|TSSN141', 'TSTORE|TSSN061');
        $last_filename = 1;

        for ($i=0; $i < count($array_product_name) ; $i++) { 
            $Product = new Product;
            $Product->sku = $this->sku($array_product_name[$i])
            $Product->name = $array_product_name[$i];
            $Product->price = $array_product_price[$i];
            $Product->stock = $faker->numberBetween($min = 10, $max = 200);
            $Product->merchant = $array_product_sku[$i];
            $Product->description = $faker->text($maxNbChars = 200);
            $Product->save();

            for ($j=0; $j < 3; $j++) { 
                $ProductImage = new ProductImage;
                $ProductImage->id_product = $Product->id;
                $ProductImage->filename = "image (". $last_filename++ .").jpg";
                $ProductImage->save();
            }
        }
    }

    public function sku($name=false) {
        $sku = substr(strtoupper(str_replace(array('a','e','i','o','u'), '', $name)), 0, 6);
        return str_replace(' ', '', $sku) . "-" .sprintf('%03u', $id);
    }	
}
