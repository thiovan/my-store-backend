<style>
* {
	box-sizing: border-box;
}

.column {
	float: left;
	width: 25%;
	padding: 10px;
}

.row:after {
	content: "";
	display: table;
	clear: both;
}
</style>

@foreach($Products as $Product)
<div class="column">
	<div class="visible-print text-center">
		{!! QrCode::size(200)->generate($Product->product_sku); !!}
		<p>{{ $Product->product_name }}</p>
	</div>
</div>
@endforeach



