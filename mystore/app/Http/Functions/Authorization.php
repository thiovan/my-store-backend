<?php

namespace App\Http\Functions;

use App\Http\Functions\CurlRequest;

class Authorization
{
	public static function isValid($request)
	{
		$params = [
			'nohp' => $request->header('nohp'),
			'token' => $request->header('token')
		];

		$response = CurlRequest::post(env('MYPAY_API_URL', null) . 'checkbalance', $params);

		if ($response['status'] == 200) {
			
			return true;

		}

		return false;

	}
}