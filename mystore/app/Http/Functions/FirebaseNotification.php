<?php

namespace App\Http\Functions;

class FirebaseNotification
{
	
	public static function send($title, $inner_title, $message, $topics)
	{
		$FIREBASE_API_KEY = 'AAAAxfrLXq8:APA91bGRZMnKeXpI45DzFwrmTq1s4GxPNVBnZE9RIFQaavCORcwa6GlfmdNpG6ZOvdv_57DAdHEMx_H7P2QnBPpCvySyClxQPEMDhdr1K9mGX6gTtRqCIciJgtWpSkWDQzsr65k7tGtU';

		$data = array
		(
			'title' => $title,
			'inner_title' => $inner_title,
			'message' => $message
		);

		$fields = array
		(
			'to'        => '/topics/' . $topics,
			'data' => $data
		);

		$headers = array
		(
			'Authorization: key=' . $FIREBASE_API_KEY,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		return $result;
	}
}