<?php

namespace App\Http\Functions;

use App\Produk;
use App\Kategori;
use App\ParentProduk;

class SkuGenerator
{
	public static function generate($id_category, $id_parent)
	{
		$Kategori = Kategori::find($id_category);
		$kategori_code = $Kategori->kode_kategori;

		$Produk = Produk::where('sku', 'like', 'MS' . $Kategori->kode_kategori . sprintf("%02d", $id_parent) . '%')->orderBy('id', 'DESC')->first();
		if ($Produk != null) {
			$increment = str_replace('MS' . $Kategori->kode_kategori . sprintf("%02d", $id_parent), '', $Produk->sku);
			$increment = (int)$increment + 1;
			return 'MS' . $Kategori->kode_kategori . sprintf("%02d", $id_parent) . $increment;
		}else{
			return 'MS' . $Kategori->kode_kategori . sprintf("%02d", $id_parent) . 1;
		}
		
	}

}