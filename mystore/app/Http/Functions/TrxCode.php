<?php

namespace App\Http\Functions;

use App\Transaction;

class TrxCode
{
	public static function generate($id_user)
	{

		if (!empty($id_user)) {

			$Transaction = Transaction::orderBy('id', 'desc')->first();

			if ($Transaction == null) {

				$id_transaction = 1;

			}else{

				$id_transaction = $Transaction->id + 1;

			}
		
			return 'TRX' . rand(1,9) . $id_transaction . time() . $id_user . rand(1,9);
			
		}else{

			return null;

		}
		
	}
}