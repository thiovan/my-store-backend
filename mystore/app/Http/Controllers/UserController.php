<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Privilege;
use App\Transaction;
use App\Produk;
use App\Merchant;
use App\Courier;

use App\Http\Resources\UserResource;

use App\Http\Functions\CurlRequest;
use App\Http\Functions\Validation;
use App\Http\Functions\Authorization;
use Carbon\Carbon;

class UserController extends BaseController
{

	public function login(Request $request)
	{

		if (Validation::isValid($request, ['nohp', 'password'])) {

			$url = env('MYPAY_API_URL', null) . 'login';
			$response = CurlRequest::post($url, $request->all());

			if ($response['status'] == 200) {

				if (User::where('nohp', $response['result']['nohp'])->count() > 0) {
					
					$User = User::where('nohp', $response['result']['nohp'])->orderBy('id', 'desc')->first();
					$User->nohp = $response['result']['nohp'];
					$User->token = $response['result']['token'];
					$User->loginTime = $response['result']['loginTime'];
					$User->nama = $response['result']['nama'];
					$User->email = $response['result']['email'];
					$User->akses = $response['result']['akses'];

					if ($User->save()) {

						$Privilege = Privilege::where('id_user', $User->id);

						if ($Privilege->update(['customer' => '1'])) {

							return $this->sendResponse('Login success', array_merge($response['result'], ['id_user'=>$User->id]));

						}else{

							return $this->sendError('Something wrong, please try again');

						}
						
					}else{

						return $this->sendError('Something wrong, please try again');

					}

				}else{

					$User = new User;
					$User->nohp = $response['result']['nohp'];
					$User->token = $response['result']['token'];
					$User->loginTime = NULL;
					$User->nama = $response['result']['nama'];
					$User->email = $response['result']['email'];
					$User->akses = $response['result']['akses'];

					if ($User->save()) {
						
						$Privilege = new Privilege;
						$Privilege->id_user = $User->id;
						$Privilege->customer = '1';
						$Privilege->merchant = '0';
						$Privilege->courier = '0';

						if ($Privilege->save()) {

							return $this->sendResponse('Login success', array_merge($response['result'], ['id_user'=>$User->id]));

						}else{

							return $this->sendError('Something wrong, please try again');

						}

					}else{

						return $this->sendError('Something wrong, please try again');

					}

				}

			}else{

				return $this->sendError('Wrong nohp or password, please check again your input');

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
		
	}

	public function balance(Request $request)
	{

		if (Authorization::isValid($request)) {

			$url = env("MYPAY_API_URL", null) . 'checkbalance';
			$params = [
				'nohp' => $request->header('nohp'),
				'token' => $request->header('token')
			];

			$response = CurlRequest::post($url, $params);

			if ($response['status'] == 200) {

				return $this->sendResponse('Balance retrieved successfully', ['balance'=>$response['balance']]);

			}else{

				return $this->sendError('Balance failed to retrieved');

			}

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}

	}

	public function showPrivilege(Request $request)
	{

		if (Validation::isValid($request, ['id_user'])) {

			if (Authorization::isValid($request)) {

				$Privilege = Privilege::where('id_user', $request->id_user)->first();
				if ($Privilege != null) {

					return $this->sendResponse('Privilege retrieved successfully', $Privilege);

				}else{

					return $this->sendError('Privilege failed to retrieved');

				}

			}else{

				return $this->sendError('Authorization failed', true, 401);

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}

	}

	public function updatePrivilege(Request $request)
	{

		if (Validation::isValid($request, ['id_user', 'key', 'value'])) {

			if (Authorization::isValid($request)) {

				$Privilege = Privilege::where('id_user', $request->id_user);

				if ($Privilege->update([$request->key => $request->value])) {

					$Privilege = Privilege::where('id_user', $request->id_user)->first();
					return $this->sendResponse('Privilege updated successfully', $Privilege);

				}else{

					return $this->sendError('Privilege failed to update');

				}

			}else{

				return $this->sendError('Authorization failed', true, 401);

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
		
	}

	public function spend(Request $request)
	{

		if (Authorization::isValid($request)) {

			$User = User::where('nohp', $request->header('nohp'))->first();

			$Transactions = Transaction::where('id_user', $User->id)->whereDate('created_at', Carbon::today())->get();

			$spendToday = 0;
			foreach ($Transactions as $Transaction) {

				$Produk = Produk::find($Transaction->id_produk);
				$spendToday = $spendToday + ($Transaction->quantity * $Produk->harga);

			}

			return $this->sendResponse('Spend today retrieved successfully', ['spend_today'=>$spendToday]);

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}

	}

	public function courierAll(Request $request)
	{

		if (Authorization::isValid($request)) {

			$Privileges = Privilege::where('courier', '1')->get();

			$courierIDs = array();
			foreach ($Privileges as $Privilege) {

				array_push($courierIDs, $Privilege->id_user);

			}

			$Users = User::find($courierIDs);

			return $this->sendResponse('Couriers retrieved successfully', UserResource::collection($Users));

		}else{

			return $this->sendError('Authorization failed', false, 401);

		}

	}

	public function courierByMerchant(Request $request)
	{

		if (Validation::isValid($request, ['id_merchant'])) {

			if (Authorization::isValid($request)) {

				$Couriers = Courier::where('id_merchant', $request->id_merchant)
				->where('is_active', 'y')
				->get();

				$courierIDs = array();
				foreach ($Couriers as $Courier) {

					array_push($courierIDs, $Courier->id_user);

				}

				$Users = User::find($courierIDs);

				return $this->sendResponse('Couriers retrieved successfully', UserResource::collection($Users));

			}else{

				return $this->sendError('Authorization failed', false, 401);

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}

	}

	public function merchantAll(Request $request)
	{
		if (Authorization::isValid($request)) {

			$Merchants = Merchant::all();
			return $this->sendResponse('Merchants retrieved successfully', $Merchants);

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

	public function mitraStatus(Request $request)
	{
		if (Authorization::isValid($request)) {

			$User = User::where('token', $request->header('token'))->first();
			$Privilege = Privilege::where('id_user', $User->id)->first();

			if ($Privilege->merchant == 1) {

				$Merchant = Merchant::where('no_telp', $User->nohp)->first();
				if ($Merchant != null) {
					
					if ($Merchant->is_active == 'y') {
						
						return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 3]);

					}else{

						return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 2]);

					}

				}else{

					return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 1]);

				}
				
			}else if($Privilege->courier == 1){

				$Courier = Courier::where('id_user', $User->id)->first();
				if ($Courier != null) {
					
					if ($Courier->is_active == 'y') {
						
						return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 3]);

					}else{

						return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 2]);

					}

				}else{

					return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 1]);

				}

			}else{

				return $this->sendResponse('Status retrieved successfully', ['status_pendaftaran' => 0]);

			}

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

}
