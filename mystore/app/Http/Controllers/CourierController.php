<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Transaction;
use App\Courier;

use App\Http\Resources\TransactionResource;

use App\Http\Controllers\Controller;

use App\Http\Functions\Authorization;
use App\Http\Functions\Validation;

use DB;
use Carbon\Carbon;

class CourierController extends BaseController
{
	public function store(Request $request)
	{
		if (Validation::isValid($request, ['id_merchant'])) {

			if (Authorization::isValid($request)) {

				$User = User::where('token', $request->header('token'))->first();

				if (Courier::where('id_user', $User->id)->count() > 0) {

					$Courier = DB::table('mystore_courier')
					->where('id_user', $User->id)
					->update(['id_merchant' => $request->id_merchant]);

					return $this->sendResponse('Courier updated successfully', Courier::where('id_user', $User->id)->first());

				}else{

					$Courier = DB::table('mystore_courier')->insert([
						[
							'id_user' => $User->id,
							'id_merchant' => $request->id_merchant,
							'created_at' => Carbon::now(),
							'updated_at' => Carbon::now()
						]
					]);

					if ($Courier) {

						return $this->sendResponse('Courier stored successfully', Courier::find(DB::getPdo()->lastInsertId()));

					}else{

						return $this->sendError('Courier failed to store');

					}

				}
				
			}else{
				
				return $this->sendError('Authorization failed', true, 401);
				
			}	

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function confirmationList(Request $request)
	{
		if (Authorization::isValid($request)) {

			$User = User::where('nohp', $request->header('nohp'))->where('token', $request->header('token'))->first();
			$Transactions = Transaction::where('id_courier', $User->id)->where('status', '<>', '3')->get();

			return $this->sendResponse('Transactions retrieved successfully', TransactionResource::collection($Transactions));

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

	public function historyList(Request $request)
	{
		if (Authorization::isValid($request)) {

			$User = User::where('nohp', $request->header('nohp'))->where('token', $request->header('token'))->first();
			$Transactions = Transaction::where('id_courier', $User->id)->where('status', '3')->get();

			return $this->sendResponse('Transactions retrieved successfully', TransactionResource::collection($Transactions));

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

	public function updateStatus(Request $request)
	{
		if (Validation::isValid($request, ['id_transaction', 'status'])) {

			$Transaction = Transaction::find($request->id_transaction);
			$Transaction->status = $request->status;

			if ($Transaction->save()) {
				
				return $this->sendResponse('Status updated successfully', new TransactionResource($Transaction));

			}else{

				return $this->sendError('Status failed to update');

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}
}
