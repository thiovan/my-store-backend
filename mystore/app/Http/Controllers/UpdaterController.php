<?php

namespace App\Http\Controllers;

use App\Version;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdaterController extends Controller
{	

	public function latestVersion()
	{

		$Version = Version::orderBy('id', 'desc')->first();

		$releaseNotes = explode('|', $Version->release_notes);

		for ($i=0; $i < count($releaseNotes); $i++) { 
			$releaseNotes[$i] = '•' . $releaseNotes[$i];
		}

		return response()->json([
			'latestVersion' 	=> $Version->latest_version,
			'latestVersionCode' => $Version->latest_version_code,
			'url' 				=> $Version->url,
			'releaseNotes' 		=> $releaseNotes
		]);

	}

}
