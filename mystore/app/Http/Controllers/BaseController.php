<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Functions\FirebaseNotification;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($message, $result)
    {
    	$response = [
            'success' => true,
            'message' => $message,
            'data'    => $result,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $object = true, $code = 202)
    {
    	if ($object) {
            $response = [
                'success' => false,
                'message' => $error,
                'data'    => (object)[]
            ];
        }else{
            $response = [
                'success' => false,
                'message' => $error,
                'data'    => array()
            ];
        }

        return response()->json($response, $code);
    }

    public function coba(Request $request)
    {
        return FirebaseNotification::send('Main title', 'asd', 'Long Message safsdfsfd, sdfsdfsdf, sdfdsfs', 'mystore-courier-081392227762');
    }
}