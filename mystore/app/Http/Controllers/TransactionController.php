<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Transaction;
use App\Produk;
use App\User;
use App\Courier;
use App\TransaksiDetail;
use App\Stok;
use App\Merchant;

use App\Http\Functions\FirebaseNotification;
use App\Http\Functions\CurlRequest;
use App\Http\Functions\Validation;
use App\Http\Functions\Authorization;
use App\Http\Functions\TrxCode;

use App\Http\Resources\TransactionResource;
use App\Http\Resources\HistoryResource;
use App\Http\Resources\ProdukResource;
use Carbon\Carbon;

class TransactionController extends BaseController
{

  public function store(Request $request)
  {

    if (Validation::isValid($request, ['trx_code', 'id_user', 'id_product', 'quantity', 'address', 'remark', 'authpinsender'])) {

      if (Authorization::isValid($request)) {

        if (empty($request->id_courier)) {
          $request->id_courier = 15;
        }

        $url = env("MYPAY_API_URL", null) . 'checkbalance';
        $params = [
          'nohp' => $request->header('nohp'),
          'token' => $request->header('token')
        ];

        $response = CurlRequest::post($url, $params);

        if ($response['status'] == 200) {

          $userBalance = $response['balance'];
          $Produk = Produk::find($request->id_product);
          $productPrice = $Produk->harga;

          if ($userBalance > ($request->quantity * $productPrice)) {

            $productStock = Stok::where('id_produk', $request->id_product)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $request->id_product)->sum('jumlah');

            if ($request->quantity <= $productStock) {

              $Transaction = new Transaction;
              $Transaction->trx_code = $request->trx_code;
              $Transaction->id_user = $request->id_user;
              $Transaction->id_produk = $request->id_product;
              $Transaction->id_courier = $request->id_courier;
              $Transaction->quantity = $request->quantity;
              $Transaction->address = $request->address;
              $Transaction->remark = $request->remark;
              if ($request->id_user == $request->id_courier) {
                $Transaction->status = '3';
              }else{
                $Transaction->status = '0';
              }
              $Transaction->save();

              $TransaksiDetail = new TransaksiDetail;
              $TransaksiDetail->id_transaksi = '999';
              $TransaksiDetail->id_produk = $Transaction->id_produk;
              $TransaksiDetail->harga = $productPrice;
              $TransaksiDetail->jumlah = $Transaction->quantity;
              $TransaksiDetail->waktu = $Transaction->created_at;
              $TransaksiDetail->machine = 'mystore-app';
              $TransaksiDetail->save();

              $url = env("MYPAY_API_URL", null) . 'payment/sendmoney';
              $params = [
                'token' => $request->header('token'),
                'reciever' => Merchant::find($Produk->id_merchant)->no_telp,
                'amount' => $Produk->harga * $Transaction->quantity,
                'authpinsender' => $request->authpinsender
              ];

              $response = CurlRequest::post($url, $params);

              if ($response['status'] == 200) {

                return $this->sendResponse('Transaction success', new TransactionResource($Transaction));
                
              }else{

                Transaction::find($Transaction->id)->delete();
                TransaksiDetail::find($TransaksiDetail->id)->delete();
                return $this->sendError('Transaction failed, please check your pin or receiver phone number');

              }

            }else{

              return $this->sendError('Product stock not enough, please reduce your product quantity');

            }

          }else{

            return $this->sendError('Your balance not enough');

          }

        }else{

          return $this->sendError('Something wrong, please try again');

        }

      }else{

        return $this->sendError('Authorization failed', true, 401);

      }

    }else{

     return $this->sendError('Parameter not satisfied');

   }
 }

 public function history(Request $request)
 {

  if (Validation::isValid($request, ['id_user', 'start_date', 'end_date'])) {

    if (Authorization::isValid($request)) {

      $start_date = \DateTime::createFromFormat('d/m/Y', $request->start_date);
      $start_date = $start_date->format('Y-m-d H:i:s');
      $end_date = \DateTime::createFromFormat('d/m/Y', $request->end_date);
      $end_date = $end_date->format('Y-m-d H:i:s');

      $Transactions = Transaction::where('id_user', $request->id_user)
      ->whereBetween('created_at', [$start_date, $end_date])
      ->orderBy('created_at', 'desc')
      ->get();

      $myTransactions = [];
      $lastTrxCode = "";
      foreach ($Transactions as $Transaction) {
        $Produk = Produk::find($Transaction->id_produk);
        $Produk = new ProdukResource($Produk);
        $Transaction = new TransactionResource($Transaction);
        
        if ($lastTrxCode === $Transaction->trx_code) {
          array_push($myTransactions[count($myTransactions)-1]['produk'], $Produk);
          array_push($myTransactions[count($myTransactions)-1]['quantity'], $Transaction->quantity);
        }else{
          $tempArray = ['transaction' => $Transaction, 'produk' => [$Produk], 'quantity' => [$Transaction->quantity]];
          array_push($myTransactions, $tempArray);
        }
        $lastTrxCode = $Transaction->trx_code;
      }

      return $this->sendResponse('Transactions retrieved successfully', $myTransactions);

    }else{

      return $this->sendError('Authorization failed', false, 401);

    }

  }else{

    return $this->sendError('Parameter not satisfied', false);

  }

}

public function generateTrxCode(Request $request)
{
  if (Validation::isValid($request, ['id_user'])) {

    if (Authorization::isValid($request)) {

      $TrxCode = TrxCode::generate($request->id_user);

      if($TrxCode){

        return $this->sendResponse('TrxCode retrieved successfully', ['trx_code' => $TrxCode]);

      }else{

        return $this->sendError('TrxCode failed to retrieved');

      }

    }else{

      return $this->sendError('Authorization failed', true, 401);

    }

  }else{

    return $this->sendError('Parameter not satisfied');

  }
}

}
