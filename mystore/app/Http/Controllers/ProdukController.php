<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Produk;
use App\Stok;
use App\Kategori;
use App\ProdukImage;
use App\ProdukDescription;
use App\ParentProduk;
use App\User;
use App\Cart;

use App\Http\Resources\ProdukResource;
use App\Http\Resources\CategoryResource;

use App\Http\Functions\Validation;
use App\Http\Functions\Authorization;
use App\Http\Functions\SkuGenerator;

use Carbon\Carbon;

class ProdukController extends BaseController
{

    public function store(Request $request)
    {
        if (Validation::isValid($request, ['name','price','stock','id_merchant','description', 'id_category', 'id_brand', 'images'])) {

            if (Authorization::isValid($request)) {

                $Produk = new Produk;
                $Produk->sku = SkuGenerator::generate($request->id_category, $request->id_brand);
                $Produk->nama_produk = $request->name;
                $Produk->harga = $request->price;
                $Produk->id_merchant = $request->id_merchant;
                $Produk->id_kategori = $request->id_category;
                $Produk->id_parent = $request->id_brand;

                if ($Produk->save()) {

                    if ($request->hasFile('images')) {
                        $index = 1;
                        foreach ($request->images as $image) {
                            $name = 'mystore_' . time().'-' . sprintf("%02d", $index) . '.' .$image->getClientOriginalExtension();
                            $destinationPath = public_path('/product_image');
                            $image->move($destinationPath, $name);

                            $ProdukImage = new ProdukImage;
                            $ProdukImage->id_produk = $Produk->id;
                            $ProdukImage->filename = $name;
                            $ProdukImage->save();
                            $index++;
                        }
                    }

                    $ProdukDescription = new ProdukDescription;
                    $ProdukDescription->id_produk = $Produk->id;
                    $ProdukDescription->deskripsi = $request->description;
                    $ProdukDescription->save();

                    $Stok = new Stok;
                    $Stok->id_produk = $Produk->id;
                    $Stok->jumlah_stok = $request->stock;
                    $Stok->waktu = Carbon::now();
                    $Stok->ket = null;
                    $Stok->save();

                    return $this->sendResponse('Product created successfully', new ProdukResource($Produk));

                }else{

                    return $this->sendError('Product failed to created');

                }

            }else{

                return $this->sendError('Authorization failed', true, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }

    }

    public function show(Request $request)
    {

        if (Validation::isValid($request, ['sku'])) {

            if (Authorization::isValid($request)) {

                $Produk = Produk::where('sku', $request->sku)->first();

                if ($Produk != null) {

                    return $this->sendResponse('Product retrieved successfully', new ProdukResource($Produk));

                }else{

                    return $this->sendError('Product Not found');

                }

            }else{

                return $this->sendError('Authorization failed', true, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }

    }

    public function search(Request $request)
    {
        if (Validation::isValid($request, ['keyword'])) {

            if (Authorization::isValid($request)) {

                if (substr($request->keyword, 0, 10) == "merchant: ") {

                    $keyword = str_replace('merchant: ', '', $request->keyword);
                    $Produks = Produk::join('merchant', 'produk.id_merchant', '=', 'merchant.id')
                    ->where('merchant.nama_merchant', 'like', '%' . $keyword . '%')
                    ->select('produk.*')
                    ->get();
                    return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Produks));

                }

                $Produks = Produk::where('nama_produk', 'like', '%' . $request->keyword . '%')->get();
                return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Produks));

            }else{

                return $this->sendError('Authorization failed', true, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }
    }

    public function category(Request $request)
    {
        if (Validation::isValid($request, ['category'])) {

            if (Authorization::isValid($request)) {

                $Produks = DB::table('produk')
                ->join('kategori', 'produk.id_kategori', '=', 'kategori.id')
                ->leftJoin('mystore_priority', 'produk.id', '=', 'mystore_priority.id_produk')
                ->select(['produk.*', 'mystore_priority.priority'])
                ->where('kategori.nama_kategori', $request->category)
                ->get();

                $Sorted = $Produks->sortByDesc('priority');

                return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Sorted));

            }else{

                return $this->sendError('Authorization failed', false, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }

    }

    public function categoryList(Request $request)
    {
        if (Authorization::isValid($request)) {

            $Kategori = Kategori::all();

            return $this->sendResponse('Categorys retrieved successfully', $Kategori);

        }else{

            return $this->sendError('Authorization failed', true, 401);

        }
    }

    public function brandList(Request $request)
    {
        if (Authorization::isValid($request)) {

            $ParentProduk = ParentProduk::all();

            return $this->sendResponse('Brands retrieved successfully', $ParentProduk);

        }else{

            return $this->sendError('Authorization failed', true, 401);

        }
    }

    public function storeCart(Request $request)
    {
        if (Validation::isValid($request, ['id_produk', 'quantity'])) {

            if (Authorization::isValid($request)) {

                $User = User::where('nohp', $request->header('nohp'))->first();

                if (Cart::where('id_produk', $request->id_produk)->count() > 0) {

                    $Cart = Cart::where('id_produk', $request->id_produk)->first();
                    $Cart->quantity = $Cart->quantity + $request->quantity;

                    if ($Cart->save()) {

                        return $this->sendResponse('Cart updated successfully', $Cart);

                    }else{

                        return $this->sendError('Cart failed to update');

                    }
                    
                }else{

                    $Cart = new Cart;
                    $Cart->id_user = $User->id;
                    $Cart->id_produk = $request->id_produk;
                    $Cart->quantity = $request->quantity;

                    if ($Cart->save()) {

                        return $this->sendResponse('Cart stored successfully', $Cart);

                    }else{

                        return $this->sendError('Cart failed to store');

                    }

                }

            }else{

                return $this->sendError('Authorization failed', true, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }
    }

    public function cartList(Request $request)
    {
        if (Authorization::isValid($request)) {

            $User = User::where('nohp', $request->header('nohp'))->first();
            $Carts = Cart::where('id_user', $User->id)->get();

            $cartArr = [];
            foreach ($Carts as $Cart) {
                array_push($cartArr, $Cart->id_produk);
            }

            $Produks = Produk::whereIn('id', $cartArr)->get();

            return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Produks));

        }else{

            return $this->sendError('Authorization failed', true, 401);

        }
    }

    public function merchant(Request $request)
    {
        if (Validation::isValid($request, ['id_merchant'])) {

            if (Authorization::isValid($request)) {

                $Produks = DB::table('produk')
                ->where('id_merchant', $request->id_merchant)
                ->get();

                return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Produks));

            }else{

                return $this->sendError('Authorization failed', false, 401);

            }

        }else{

            return $this->sendError('Parameter not satisfied');

        }
    }

    // public function qrcode()
    // {
    //     $Products = Product::all();

    //     return view('qrcode', compact('Products'));
    // }
}
