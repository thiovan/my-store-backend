<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Functions\Validation;
use App\Kategori;
use App\Merchant;
use App\ParentProduk;
use App\Produk;
use App\Stok;
use App\TransaksiDetail;

class TstoreController extends BaseController
{
	public function storeKategori(Request $request)
	{
		if (Validation::isValid($request, ['nama_kategori', 'kode_kategori'])) {

			$Kategori = new Kategori;
			$Kategori->nama_kategori = $request->nama_kategori;
			$Kategori->kode_kategori = $request->kode_kategori;

			if ($Kategori->save()) {
				return $this->sendResponse('Kategori stored successfully', $Kategori);
			}else{
				return $this->sendError('Kategori failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateKategori(Request $request, $id)
	{
		if (Validation::isValid($request, ['nama_kategori', 'kode_kategori'])) {

			$Kategori = Kategori::find($id);
			$Kategori->nama_kategori = $request->nama_kategori;
			$Kategori->kode_kategori = $request->kode_kategori;

			if ($Kategori->save()) {
				return $this->sendResponse('Kategori updated successfully', $Kategori);
			}else{
				return $this->sendError('Kategori failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteKategori($id)
	{
		if (Kategori::destroy($id)) {
			return $this->sendResponse('Kategori deleted successfully', '');
		}else{
			return $this->sendError('Kategori failed to delete', true, 204);
		}
	}

	public function storeMerchant(Request $request)
	{
		if (Validation::isValid($request, ['username', 'password', 'nama_merchant', 'no_telp', 'address', 'is_active', 'email'])) {

			$Merchant = new Merchant;
			$Merchant->username = $request->username;
			$Merchant->password = $request->password;
			$Merchant->nama_merchant = $request->nama_merchant;
			$Merchant->no_telp = $request->no_telp;
			$Merchant->address = $request->address;
			$Merchant->is_active = $request->is_active;

			if ($Merchant->save()) {
				return $this->sendResponse('Merchant stored successfully', $Merchant);
			}else{
				return $this->sendError('Merchant failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateMerchant(Request $request, $id)
	{
		if (Validation::isValid($request, ['username', 'password', 'nama_merchant', 'no_telp', 'address', 'is_active', 'email'])) {

			$Merchant = Merchant::find($id);
			$Merchant->username = $request->username;
			$Merchant->password = $request->password;
			$Merchant->nama_merchant = $request->nama_merchant;
			$Merchant->no_telp = $request->no_telp;
			$Merchant->address = $request->address;
			$Merchant->is_active = $request->is_active;

			if ($Merchant->save()) {
				return $this->sendResponse('Merchant updated successfully', $Merchant);
			}else{
				return $this->sendError('Merchant failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteMerchant($id)
	{
		if (Merchant::destroy($id)) {
			return $this->sendResponse('Merchant deleted successfully', '');
		}else{
			return $this->sendError('Merchant failed to delete', true, 204);
		}
	}

	public function storeParent(Request $request)
	{
		if (Validation::isValid($request, ['nama_parent', 'id_kategori', 'id_merchant'])) {

			$ParentProduk = new ParentProduk;
			$ParentProduk->nama_parent = $request->nama_parent;
			$ParentProduk->id_kategori = $request->id_kategori;
			$ParentProduk->id_merchant = $request->id_merchant;

			if ($ParentProduk->save()) {
				return $this->sendResponse('Parent stored successfully', $ParentProduk);
			}else{
				return $this->sendError('Parent failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateParent(Request $request, $id)
	{
		if (Validation::isValid($request, ['nama_parent', 'id_kategori', 'id_merchant'])) {

			$ParentProduk = ParentProduk::find($id);
			$ParentProduk->nama_parent = $request->nama_parent;
			$ParentProduk->id_kategori = $request->id_kategori;
			$ParentProduk->id_merchant = $request->id_merchant;

			if ($ParentProduk->save()) {
				return $this->sendResponse('Parent updated successfully', $ParentProduk);
			}else{
				return $this->sendError('Parent failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteParent($id)
	{
		if (ParentProduk::destroy($id)) {
			return $this->sendResponse('Parent deleted successfully', '');
		}else{
			return $this->sendError('Parent failed to delete', true, 204);
		}
	}

	public function storeProduk(Request $request)
	{
		if (Validation::isValid($request, ['sku', 'nama_produk', 'harga', 'id_merchant', 'id_kategori', 'id_parent'])) {

			$Produk = new Produk;
			$Produk->sku = $request->sku;
			$Produk->nama_produk = $request->nama_produk;
			$Produk->harga = $request->harga;
			$Produk->id_merchant = $request->id_merchant;
			$Produk->id_kategori = $request->id_kategori;
			$Produk->id_parent = $request->id_parent;

			if ($Produk->save()) {
				return $this->sendResponse('Produk stored successfully', $Produk);
			}else{
				return $this->sendError('Produk failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateProduk(Request $request, $id)
	{
		if (Validation::isValid($request, ['sku', 'nama_produk', 'harga', 'id_merchant', 'id_kategori', 'id_parent'])) {

			$Produk = Produk::find($id);
			$Produk->sku = $request->sku;
			$Produk->nama_produk = $request->nama_produk;
			$Produk->harga = $request->harga;
			$Produk->id_merchant = $request->id_merchant;
			$Produk->id_kategori = $request->id_kategori;
			$Produk->id_parent = $request->id_parent;

			if ($Produk->save()) {
				return $this->sendResponse('Produk updated successfully', $Produk);
			}else{
				return $this->sendError('Produk failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteProduk($id)
	{
		if (Produk::destroy($id)) {
			return $this->sendResponse('Produk deleted successfully', '');
		}else{
			return $this->sendError('Produk failed to delete', true, 204);
		}
	}

	public function storeStok(Request $request)
	{
		if (Validation::isValid($request, ['id_produk', 'jumlah_stok', 'waktu', 'ket'])) {

			$Stok = new Stok;
			$Stok->id_produk = $request->id_produk;
			$Stok->jumlah_stok = $request->jumlah_stok;
			$Stok->waktu = $request->waktu;
			$Stok->ket = $request->ket;

			if ($Stok->save()) {
				return $this->sendResponse('Stok stored successfully', $Stok);
			}else{
				return $this->sendError('Stok failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateStok(Request $request, $id)
	{
		if (Validation::isValid($request, ['id_produk', 'jumlah_stok', 'waktu', 'ket'])) {

			$Stok = Stok::find($id);
			$Stok->id_produk = $request->id_produk;
			$Stok->jumlah_stok = $request->jumlah_stok;
			$Stok->waktu = $request->waktu;
			$Stok->ket = $request->ket;

			if ($Stok->save()) {
				return $this->sendResponse('Stok updated successfully', $Stok);
			}else{
				return $this->sendError('Stok failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteStok($id)
	{
		if (Stok::destroy($id)) {
			return $this->sendResponse('Stok deleted successfully', $Stok);
		}else{
			return $this->sendError('Stok failed to delete', true, 204);
		}
	}

	public function storeTransaksidetail(Request $request)
	{
		if (Validation::isValid($request, ['id_transaksi', 'id_produk', 'harga', 'jumlah', 'waktu', 'machine'])) {

			$TransaksiDetail = new TransaksiDetail;
			$TransaksiDetail->id_transaksi = $request->id_transaksi;
			$TransaksiDetail->id_produk = $request->id_produk;
			$TransaksiDetail->harga = $request->harga;
			$TransaksiDetail->jumlah = $request->jumlah;
			$TransaksiDetail->waktu = $request->waktu;
			$TransaksiDetail->machine = $request->machine;

			if ($TransaksiDetail->save()) {
				return $this->sendResponse('Transaksi Detail stored successfully', $TransaksiDetail);
			}else{
				return $this->sendError('Transaksi Detail failed to store', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function updateTransaksidetail(Request $request, $id)
	{
		if (Validation::isValid($request, ['id_transaksi', 'id_produk', 'harga', 'jumlah', 'waktu', 'machine'])) {

			$TransaksiDetail = TransaksiDetail::find($id);
			$TransaksiDetail->id_transaksi = $request->id_transaksi;
			$TransaksiDetail->id_produk = $request->id_produk;
			$TransaksiDetail->harga = $request->harga;
			$TransaksiDetail->jumlah = $request->jumlah;
			$TransaksiDetail->waktu = $request->waktu;
			$TransaksiDetail->machine = $request->machine;

			if ($TransaksiDetail->save()) {
				return $this->sendResponse('Transaksi Detail updated successfully', $TransaksiDetail);
			}else{
				return $this->sendError('Transaksi Detail failed to update', true, 204);
			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function deleteTransaksidetail($id)
	{
		if (TransaksiDetail::destroy($id)) {
			return $this->sendResponse('Transaksi Detail deleted successfully', $TransaksiDetail);
		}else{
			return $this->sendError('Transaksi Detail failed to delete', true, 204);
		}
	}
}
