<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Merchant;
use App\Produk;
use App\User;
use App\Transaction;

use App\Http\Resources\ProdukResource;
use App\Http\Resources\TransactionResource;

use App\Http\Functions\CurlRequest;
use App\Http\Functions\Validation;
use App\Http\Functions\Authorization;

use DB;

class MerchantController extends BaseController
{
	public function store(Request $request)
	{
		if (Validation::isValid($request, ['nama_merchant', 'address'])) {

			if (Authorization::isValid($request)) {

				$User = User::where('nohp', $request->header('nohp'))->first();
				if (Merchant::where('username', 'mystore_merchant' . $User->id)->count() > 0) {

					$Merchant = Merchant::where('username', "mystore_merchant" . $User->id)->first();
					$Merchant->username = 'mystore_merchant' . $User->id;
					$Merchant->password = '-';
					$Merchant->nama_merchant = $request->nama_merchant;
					$Merchant->no_telp = $request->header('nohp');
					$Merchant->address = $request->address;
					$Merchant->is_active = 'n';
					$Merchant->email = null;

					if ($Merchant->save()) {

						return $this->sendResponse('Merchant updated successfully', $Merchant);

					}else{

						return $this->sendError('Merchant failed to update');

					}
					
				}else{

					$Merchant = new Merchant;
					$Merchant->username = "mystore_merchant" . $User->id;
					$Merchant->password = "-";
					$Merchant->nama_merchant = $request->nama_merchant;
					$Merchant->no_telp = $request->header('nohp');
					$Merchant->address = $request->address;
					$Merchant->is_active = 'n';
					$Merchant->email = null;

					if ($Merchant->save()) {

						return $this->sendResponse('Merchant stored successfully', $Merchant);

					}else{

						return $this->sendError('Merchant failed to store');

					}

				}

			}else{

				return $this->sendError('Authorization failed', true, 401);

			}

		}else{

			return $this->sendError('Parameter not satisfied');

		}
	}

	public function productList(Request $request)
	{
		
		if (Authorization::isValid($request)) {

			$Merchant = Merchant::where('no_telp', $request->header('nohp'))->orderBy('id', 'DESC')->first();
			$Produks = Produk::where('id_merchant', $Merchant->id)->get();

			return $this->sendResponse('Products retrieved successfully', ProdukResource::collection($Produks));

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}

	}

	public function historyList(Request $request)
	{
		if (Authorization::isValid($request)) {

			$Merchant = Merchant::where('no_telp', $request->header('nohp'))->first();
			$Produks = Produk::where('id_merchant', $Merchant->id)->get();
			$produkArr = [];
			foreach ($Produks as $Produk) {
				array_push($produkArr, $Produk->id);
			}

			$Transactions = Transaction::whereIn('id_produk', $produkArr)->orderBy('created_at', 'DESC')->get();

			return $this->sendResponse('Transactions retrieved successfully', TransactionResource::collection($Transactions));

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

	public function checkMerchant(Request $request)
	{
		if (Authorization::isValid($request)) {

			$Merchant = Merchant::where('no_telp', $request->header('nohp'))->count();

			if ($Merchant > 0) {
				
				return $this->sendResponse('Merchant registered', ['registered' => true]);

			}else{

				return $this->sendResponse('Merchant not registered', ['registered' => false]);

			}

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}

	public function confirmationList(Request $request)
	{
		if (Authorization::isValid($request)) {

			$Merchant = Merchant::where('no_telp', $request->header('nohp'))->first();

			$Transactions = DB::table('mystore_transaction')
			->join('produk', 'mystore_transaction.id_produk', '=', 'produk.id')
			->where('produk.id_merchant', $Merchant->id)
			->where('mystore_transaction.status', '0')
			->select('mystore_transaction.*')
			->get();

			$myTransactions = [];
			$lastTrxCode = "";
			foreach ($Transactions as $Transaction) {
				$Produk = Produk::find($Transaction->id_produk);
				$Produk = new ProdukResource($Produk);
				$Transaction = new TransactionResource($Transaction);

				if ($lastTrxCode === $Transaction->trx_code) {
					array_push($myTransactions[count($myTransactions)-1]['produk'], $Produk);
					array_push($myTransactions[count($myTransactions)-1]['quantity'], $Transaction->quantity);
				}else{
					$tempArray = ['transaction' => $Transaction, 'produk' => [$Produk], 'quantity' => [$Transaction->quantity]];
					array_push($myTransactions, $tempArray);
				}
				$lastTrxCode = $Transaction->trx_code;
			}

			return $this->sendResponse('Transactions retrieved successfully', $myTransactions);

		}else{

			return $this->sendError('Authorization failed', true, 401);

		}
	}
}
