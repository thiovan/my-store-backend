<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProdukImage;
use App\Http\Controllers\Controller;
use File;
use Response;
use Intervention\Image\ImageManagerStatic as Image;

class ProductImageController extends Controller
{
	public function show($id_image)
	{
		$id_image = substr($id_image, 3);
		$id_image = substr($id_image, 0, -2);
		$img = ProdukImage::where('id', $id_image)->first()->filename;
		$path = public_path('product_image') . '/' . $img;

		if (File::exists($path)) {

			// $img = Image::make($path);
			// $img->resize(768, 500);
			// return $img->response();

			$file = File::get($path);
			$type = File::mimeType($path);
			$response = Response::make($file, 200);
			$response->header("Content-Type", $type);
			return $response;


		}else{
			return "Image Not Found";
		}
	}
}
