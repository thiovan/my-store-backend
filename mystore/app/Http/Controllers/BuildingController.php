<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use App\Http\Resources\BuildingResource;
use App\Http\Controllers\Controller;
use App\Http\Functions\Authorization;

class BuildingController extends BaseController
{
	public function buildingAll(Request $request)
	{
		
		if (Authorization::isValid($request)) {

			$Buildings = Building::all();

			return $this->sendResponse('Buildings retrieved successfully', BuildingResource::collection($Buildings));

		}else{

			return $this->sendError('Authorization failed', false, 401);

		}

	}
}
