<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DB;
use App\Produk;
use App\User;
use App\Transaction;
use App\Http\Resources\ProdukResource;
use App\Http\Resources\UserResource;

class historyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Transactions = Transaction::where('trx_code', $this->trx_code)->get();
        $idProduks = [];
        foreach ($Transactions as $Transaction) {
            array_push($idProduks, $Transaction->id_produk);
        }

        dd($this);

        return [
            'id'            => $this->id,
            'trx_code'      => $this->trx_code,
            'user'          => new UserResource(User::find($this->id_user)),
            'product'       => ProdukResource::collection(DB::table('produk')->whereIn('id', $idProduks)->get()),
            'courier'       => new UserResource(User::find($this->id_courier)),
            'quantity'      => (int) $this->quantity,
            'address'       => $this->address,
            'remark'        => $this->remark,
            'status'        => (int) $this->status,
            'date'          => (string) $this->created_at,
        ];
    }
}
