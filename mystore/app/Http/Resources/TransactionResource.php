<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Produk;
use App\User;
use App\Http\Resources\ProdukResource;
use App\Http\Resources\UserResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'trx_code'      => $this->trx_code,
            'user'          => new UserResource(User::find($this->id_user)),
            'product'       => new ProdukResource(Produk::find($this->id_produk)),
            'courier'       => new UserResource(User::find($this->id_courier)),
            'quantity'      => (int) $this->quantity,
            'address'       => $this->address,
            'remark'        => $this->remark,
            'status'        => (int) $this->status,
            'date'          => (string) $this->created_at,
        ];
    }
}
