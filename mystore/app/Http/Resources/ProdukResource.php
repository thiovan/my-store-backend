<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Merchant;
use App\Kategori;
use App\Stok;
use App\ParentProduk;
use App\ProdukImage;
use App\ProdukDescription;
use App\TransaksiDetail;

class ProdukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (ProdukDescription::where('id_produk', $this->id)->count() > 0) {
            $description = ProdukDescription::where('id_produk', $this->id)->first()->deskripsi;
        }else{
            $description = null;
        }

        return [
            'id'            => $this->id,
            'sku'           => $this->sku,
            'name'          => $this->nama_produk,
            'price'         => $this->harga,
            'stock'         => Stok::where('id_produk', $this->id)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $this->id)->sum('jumlah'),
            'merchant'      => Merchant::find($this->id_merchant),
            'kategori'      => Kategori::find($this->id_kategori),
            'parent'        => ParentProduk::find($this->id_parent),
            'description'   => $description,
            'image'         => ProdukImageResource::collection(
                ProdukImage::where('id_produk', $this->id)->get()
            ),
        ];
    }
}
