<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdukImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'id_produk'     => $this->id_produk,
            'filename'      => url('product/image/' . rand(200,800) . $this->id . rand(20,80))
            //'filename'      => asset('product_image/' . str_replace(' ', '%20', $this->filename))
            // asset('product_image/' . str_replace(' ', '%20', $this->filename))
            // url('product/image/' . $this->id_produk)
        ];
    }
}
