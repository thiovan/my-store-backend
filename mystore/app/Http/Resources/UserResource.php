<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_user'        => $this->id,
            'nohp'      => $this->nohp,
            'token'     => $this->token,
            'loginTime' => $this->loginTime,
            'nama'      => $this->nama,
            'email'     => $this->email,
            'akses'     => $this->akses,
        ];
    }
}
