<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
	use SoftDeletes;

    protected $table = 'mystore_user';
    protected $dates = ['deleted_at'];
}
