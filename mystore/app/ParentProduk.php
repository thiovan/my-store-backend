<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentProduk extends Model
{
    protected $table = 'parent';
    public $timestamps = false;
}
