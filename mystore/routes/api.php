<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//User
Route::post('user/login', 'UserController@login');
Route::post('user/show/privilege', 'UserController@showPrivilege');
Route::post('user/update/privilege', 'UserController@updatePrivilege');
Route::post('user/balance', 'UserController@balance');
Route::post('user/spend', 'UserController@spend');
Route::post('user/courier/all', 'UserController@courierAll');
Route::post('user/courier/merchant', 'UserController@courierByMerchant');
Route::post('user/merchant/all', 'UserController@merchantAll');
Route::post('user/mitra/status', 'UserController@mitraStatus');

//Product
Route::post('product/store', 'ProdukController@store');
Route::post('product/show', 'ProdukController@show');
Route::post('product/search', 'ProdukController@search');
Route::post('product/category', 'ProdukController@category');
Route::post('product/category/list', 'ProdukController@categoryList');
Route::post('product/brand/list', 'ProdukController@brandList');
Route::post('product/merchant', 'ProdukController@merchant');
Route::post('product/cart', 'ProdukController@storeCart');		//dev
Route::post('product/cart/list', 'ProdukController@cartList');	//dev

//Transaction
Route::post('transaction/store', 'TransactionController@store');
Route::post('transaction/history', 'TransactionController@history');
Route::post('transaction/trxcode', 'TransactionController@generateTrxCode');

//Building
Route::post('building/all', 'BuildingController@buildingAll');

//Merchant
Route::post('merchant/store', 'MerchantController@store');
Route::post('merchant/product', 'MerchantController@productList');
Route::post('merchant/history', 'MerchantController@historyList');
Route::post('merchant/check', 'MerchantController@checkMerchant');
Route::post('merchant/confirmation', 'MerchantController@confirmationList');

//Courier
Route::post('courier/store', 'CourierController@store');
Route::post('courier/confirmation', 'CourierController@confirmationList');
Route::post('courier/history', 'CourierController@historyList');
Route::post('courier/status', 'CourierController@updateStatus');

//App latest
Route::any('version/latest', 'UpdaterController@latestVersion');

//Bridge TStore
//Insert
// Route::post('tstore/kategori', 'TstoreController@storeKategori');
// Route::post('tstore/merchant', 'TstoreController@storeMerchant');
// Route::post('tstore/parent', 'TstoreController@storeParent');
// Route::post('tstore/produk', 'TstoreController@storeProduk');
// Route::post('tstore/stok', 'TstoreController@storeStok');
// Route::post('tstore/transaksidetail', 'TstoreController@storeTransaksidetail');

//Update
// Route::put('tstore/kategori/{id}', 'TstoreController@updateKategori');
// Route::put('tstore/merchant/{id}', 'TstoreController@updateMerchant');
// Route::put('tstore/parent/{id}', 'TstoreController@updateParent');
// Route::put('tstore/produk/{id}', 'TstoreController@updateProduk');
// Route::put('tstore/stok/{id}', 'TstoreController@updateStok');
// Route::put('tstore/transaksidetail/{id}', 'TstoreController@updateTransaksidetail');

//Delete
// Route::delete('tstore/kategori/{id}', 'TstoreController@deleteKategori');
// Route::delete('tstore/merchant/{id}', 'TstoreController@deleteMerchant');
// Route::delete('tstore/parent/{id}', 'TstoreController@deleteParent');
// Route::delete('tstore/produk/{id}', 'TstoreController@deleteProduk');
// Route::delete('tstore/stok/{id}', 'TstoreController@deleteStok');
// Route::delete('tstore/transaksidetail/{id}', 'TstoreController@deleteTransaksidetail');

//Coba
Route::any('coba', 'BaseController@coba');